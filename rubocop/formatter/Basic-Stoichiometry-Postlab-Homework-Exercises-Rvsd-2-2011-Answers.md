## Basic Stoichiometry Post-lab Homework Exercises Rvsd 2 2011 Answers

 
 
 
 
**Download File > [https://tinurli.com/2tDeiI](https://tinurli.com/2tDeiI)**

 
 
 
 
 
# How to Solve Basic Stoichiometry Post-Lab Homework Exercises
 
Stoichiometry is the study of the quantitative relationships between the reactants and products in a chemical reaction. It allows us to predict how much of a substance is consumed or produced by a reaction, based on the balanced chemical equation and the amount of another substance involved.
 
One of the most common types of stoichiometry problems is finding the mole ratio, which relates the amounts in moles of any two substances in a chemical reaction[^2^]. We can write a mole ratio for a pair of substances by looking at the coefficients in front of each species in the balanced chemical equation. For example, consider the following reaction:
 
![](https://latex.codecogs.com/png.latex?2H_2&plus;O_2\rightarrow&space;2H_2O "2H_2+O_2\rightarrow 2H_2O")
 
The mole ratio of hydrogen to oxygen is 2:1, meaning that for every 2 moles of hydrogen that react, 1 mole of oxygen is consumed. Similarly, the mole ratio of hydrogen to water is 2:2, or 1:1, meaning that for every mole of hydrogen that reacts, one mole of water is produced.
 
To solve basic stoichiometry post-lab homework exercises, we need to use the mole ratio and the given information to find the unknown quantity. Here are some steps to follow:
 
1. Identify the given and unknown substances and their units.
2. Write a balanced chemical equation for the reaction.
3. Write the mole ratio for the given and unknown substances from the coefficients in the equation.
4. Use dimensional analysis to convert the given amount to moles, if necessary.
5. Multiply the given amount in moles by the mole ratio to find the unknown amount in moles.
6. Convert the unknown amount in moles to the desired unit, if necessary.

Let's look at an example from basic stoichiometry post-lab homework exercises rvsd 2 2011 answers[^1^]:
 
**Example:** How many grams of sodium sulfate will be formed when you start with 200 grams of sodium hydroxide and you have an excess of sulfuric acid (H<sub>2</sub>SO<sub>4</sub>)? Show all work.
 
**Solution:**

- The given substance is sodium hydroxide (NaOH) and its amount is 200 g. The unknown substance is sodium sulfate (Na<sub>2</sub>SO<sub>4</sub>) and its amount in grams is what we need to find.
- The balanced chemical equation for the reaction is:
![](https://latex.codecogs.com/png.latex?2NaOH&plus;H_2SO_4\rightarrow&space;Na_2SO_4&plus;2H_2O "2NaOH+H_2SO_4\rightarrow Na_2SO_4+2H_2O")
- The mole ratio of NaOH to Na<sub>2</sub>SO<sub>4</sub> is 2:1, meaning that for every 2 moles of NaOH that react, 1 mole of Na<sub>2</sub>SO<sub>4</sub> is produced.
- We need to convert 200 g of NaOH to moles using its molar mass, which is 40 g/mol:
![](https://latex.codecogs.com/png.latex?\frac200&space;g&space;NaOH1\times\frac1&space;mol&space;NaOH40&space;g&space;NaOH=5&space;mol&space;NaOH "\frac200 g NaOH1\times\frac1 mol NaOH40 g NaOH=5 mol NaOH")
- We multiply 5 mol of NaOH by the mole ratio to find the moles of Na<sub>2</sub>SO<sub>4</sub>:
![]() dde7e20689




